+++
title = "About Us"
+++

> # **Introduction**
NUTN ISC (National University of Tainan Information Security Club) is officially known as the National University of Tainan Information Security Research Club. It was founded by a group of students from [National University of Tainan](https://www.nutn.edu.tw/) who are passionate about researching information security technologies.

We aim to bring together more enthusiasts in information security through activities such as study groups, collaborative research, and team training to level up together (＞ v ＜). Additionally, we organize security courses and lectures to guide interested students into the field of information security.


> # **Histories**