+++
title = "Home"
insert_anchor_links = "left"
+++

{% crt() %}
```

  _   _      _   _   _____    _   _                   ____       ____  
 | \ |"|  U |"|u| | |_ " _|  | \ |"|        ___     / __"| u U /"___| 
<|  \| |>  \| |\| |   | |   <|  \| |>      |_"_|   <\___ \/  \| | u   
U| |\  |u   | |_| |  /| |\  U| |\  |u       | |     u___) |   | |/__  
 |_| \_|   <<\___/  u |_|U   |_| \_|      U/| |\u   |____/>>   \____| 
 ||   \\,-(__) )(   _// \\_  ||   \\,-  -,_|___|_,-  )(  (__) _// \\  
 (_")  (_/    (__) (__) (__) (_")  (_/  \_)-' '-(_/ (__)     (__)(__) 


```
{% end %}

> # **最新消息**

- 2025-02-17 更新網站模板至 [_Duckquill 6.1.1_](https://codeberg.org/daudix/duckquill/releases/tag/v6.1.1)。

- 2024-09-19 更新網站模板至 [_Duckquill 5.0.0_](https://codeberg.org/daudix/duckquill/releases/tag/v5.0.0)。

- 2024-09-15 成員有座山，山上有個人～[來去看看->](members/#gong-guan)

- 2024-09-11 成員頁面混入了奇怪的生物？！[來去看看->](members/#cai-wu)

- 2024-09-08 成員頁面解鎖了新角色！[來去看看->](members/#fu-she-chang)

- 2024-09-01 更新了[成員](@/members/index.md)頁面。

- 2024-08-31 更新網站模板至 [_Duckquill 4.8.0_](https://codeberg.org/daudix/duckquill/releases/tag/v4.8.0)。

- 2024-08-29 更新網站模板至 [_Duckquill 4.7.1_](https://codeberg.org/daudix/duckquill/releases/tag/v4.7.1)。

- 2024-08-01 歡慶台南大學資安社正式成立。
