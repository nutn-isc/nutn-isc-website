+++
title = "Home"
insert_anchor_links = "left"
+++

{% crt() %}
```

  _   _      _   _   _____    _   _                   ____       ____  
 | \ |"|  U |"|u| | |_ " _|  | \ |"|        ___     / __"| u U /"___| 
<|  \| |>  \| |\| |   | |   <|  \| |>      |_"_|   <\___ \/  \| | u   
U| |\  |u   | |_| |  /| |\  U| |\  |u       | |     u___) |   | |/__  
 |_| \_|   <<\___/  u |_|U   |_| \_|      U/| |\u   |____/>>   \____| 
 ||   \\,-(__) )(   _// \\_  ||   \\,-  -,_|___|_,-  )(  (__) _// \\  
 (_")  (_/    (__) (__) (__) (_")  (_/  \_)-' '-(_/ (__)     (__)(__) 


```
{% end %}

> # **Latest News**

- 2025-02-17 Website template upgraded to [_Duckquill 6.1.1_](https://codeberg.org/daudix/duckquill/releases/tag/v6.1.1).

- 2024-09-19 Website template upgraded to [_Duckquill 5.0.0_](https://codeberg.org/daudix/duckquill/releases/tag/v5.0.0).

- 2024-09-15 A boss sitting on a mountain. [Check it out->](members/#public-relations-officer)

- 2024-09-11 A "fantasy" creature just appeared! [Check it out->](members/#treasurer)

- 2024-09-08 New character has been unlocked! [Check it out->](members/#vice-president)

- 2024-09-01 Updated [Members](@/members/index.en.md) page。

- 2024-08-31 Website template upgraded to [_Duckquill 4.8.0_](https://codeberg.org/daudix/duckquill/releases/tag/v4.8.0).

- 2024-08-29 Website template upgraded to [_Duckquill 4.7.1_](https://codeberg.org/daudix/duckquill/releases/tag/v4.7.1).

- 2024-08-01 Let’s celebrate the official founding of the National University of Tainan Information Security Club (NUTN ISC)! 🎉