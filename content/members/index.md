+++
title = "成員"
+++


> ## **社長**
<div>
    <!-- Aside block -->
    <aside>
        <span style="font-size: larger;">hicat0x0</span>
        {{
            image(
                url      = "images/president_sleep.png",
                alt      = "President",
                no_hover = true
            )           
        }}
        <span style="color: gray;">
            開天闢地，初始の社長
        </span>
    </aside>
    <!-- Description -->
    <dl>
        <dt style="color: gray;">姓名</dt>
        <dd>
            hicat0x0
        <dd>
        <dt style="color: gray;">研究領域</dt>
        <dd>
            資訊安全、工控安全、反編譯工程
        <dd>
        <dt style="color: gray;">介紹<dt>
        <dd>
            南大資安社的創立者，他的名字將在
            <a href="https://csie.nutn.edu.tw/"> 南大資工系 </a>
            的歷史中寫下全新的篇章。他不是一個普通的學生，而是一位才華橫溢的天才，被譽為資工系百年來最具潛力的奇才。為了在這個數位時代保護資訊安全，並讓更多志同道合的同學們能夠在這個領域中展翅高飛，他毅然決然地創立了南大資安社。
        </dd>
    </dl>
<br/><br/><br/><br/><br/><br/>
</div>



> ## **副社長**
<div>
    <!-- Aside block -->
    <aside>
        <span style="font-size: larger;">SpringRain</span>
        {{
            image(
                url      = "images/vice_president.jpeg",
                alt      = "Vice President",
                no_hover = true
            )           
        }}
        <span style="color: gray;">
            自家養的新葉喵（不會後空翻），滿一周歲。
        </span>
    </aside>
    <!-- Description -->
    <dl>
        <dt style="color: gray;">姓名</dt>
        <dd>
            SpringRain
        <dd>
        <dt style="color: gray;">研究領域</dt>
        <dd>
            機器學習（一些）、資訊安全（不是很多）
        <dd>
        <dt style="color: gray;">介紹<dt>
        <dd>
            大二寒假期間巧合之下接觸到資安並對此有興趣，與 hicat0x0 共同創辦南大資安社。喜歡寫記錄
        </dd>
    </dl>
<br/><br/><br/><br/><br/><br/>
</div>



> ## **公關**
<div>
    <!-- Aside block -->
    <aside>
        <span style="font-size: larger;">山高</span>
        {{
            image(
                url      = "images/known.png",
                alt      = "Public Relations Officer",
                no_hover = true
            )           
        }}
        <span style="color: gray;">
            啊啊啊啊啊啊啊啊啊!
        </span>
    </aside>
    <!-- Description -->
    <dl>
        <dt style="color: gray;">姓名</dt>
        <dd>
            山高
        <dd>
        <dt style="color: gray;">研究領域</dt>
        <dd>
            平面設計、數位訊號處理
        <dd>
        <dt style="color: gray;">介紹<dt>
        <dd>
            一座很高很高的山
        </dd>
    </dl>
<br/><br/><br/><br/><br/><br/>
</div>



> ## **財務**
<div>
    <!-- Aside block -->
    <aside>
        <span style="font-size: larger;">Kami</span>
        {{
            image(
                url      = "images/treasurer_zh.png",
                alt      = "treasurer",
                no_hover = true
            )           
        }}
        <span style="color: gray;">
            和我簽訂契約成為AC少年（女）吧！
        </span>
    </aside>
    <!-- Description -->
    <dl>
        <dt style="color: gray;">姓名</dt>
        <dd>
            Kami
        <dd>
        <dt style="color: gray;">研究領域</dt>
        <dd>
            演算法（一點點）、付錢給OpenAI
        <dd>
        <dt style="color: gray;">介紹<dt>
        <dd>
            喜歡酪梨多過競程，被動報名成為幹部
        </dd>
    </dl>
<br/><br/><br/><br/><br/><br/>
</div>



> ## **網管**
<div>
    <!-- Aside block -->
    <aside>
        <span style="font-size: larger;">Sam Doner</span>
        {{
            image(
                url      = "images/doner.png",
                alt      = "MIS",
                no_hover = true
            )           
        }}
        <span style="color: gray;">
            這是土耳其烤肉捲餅(
                <a href="https://zh.wikipedia.org/zh-tw/%E5%9C%9F%E8%80%B3%E5%85%B6%E7%83%A4%E8%82%89">
                Döner
                </a>
            )，不是墨西哥捲餅(
                <a href="https://zh.wikipedia.org/zh-tw/%E5%9C%9F%E8%80%B3%E5%85%B6%E7%83%A4%E8%82%89">
                Burrito
                </a>
            )
        </span>
    </aside>
    <!-- Description -->
    <dl>
        <dt style="color: gray;">姓名</dt>
        <dd>
            Sam Doner
        <dd>
        <dt style="color: gray;">研究領域</dt>
        <dd>
            睡覺
        <dd>
        <dt style="color: gray;">介紹<dt>
        <dd>
            這傢伙很懶，別理他。
        </dd>
    </dl>
<br/><br/><br/><br/><br/><br/>
</div>




<!-- Buttons -->
<div class="dialog-buttons">
  <a class="inline-button" href="#top">Go to Top</a>
</div>