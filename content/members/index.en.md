+++
title = "Members"
+++


> ## **Club President**
<div>
    <!-- Aside block -->
    <aside>
        <span style="font-size: larger;">hicat0x0</span>
        {{
            image(
                url      = "images/president_sleep.png",
                alt      = "President",
                no_hover = true
            )           
        }}
        <span style="color: gray;">
            Burn the light, slain the darkness. Yumm
        </span>
    </aside>
    <!-- Description -->
    <dl>
        <dt style="color: gray;">Name</dt>
        <dd>
            hicat0x0
        <dd>
        <dt style="color: gray;">Field of Study</dt>
        <dd>
            Information Security, OT Security, Decompilation Engineering
        <dd>
        <dt style="color: gray;">Intro<dt>
        <dd>
            In the quiet halls of
            <a href="https://csie.nutn.edu.tw/">CSIE NUTN</a>,
            one student stood apart. His name was 于京平. With no group to fuel his passion for information security, he forged his own path. Alone, he founded the first Information Security Club, driven by sheer will and a thirst for knowledge. This club became a sanctuary for those who sought to protect the digital world, a beacon of innovation in a sea of tradition. 于京平’s single-handed determination turned a dream into a legacy.
        </dd>
    </dl>
<br/><br/><br/><br/><br/><br/>
</div>



> ## **Vice President**
<div>
    <!-- Aside block -->
    <aside>
        <span style="font-size: larger;">SpringRain</span>
        {{
            image(
                url      = "images/vice_president.jpeg",
                alt      = "Vice President",
                no_hover = true
            )           
        }}
        <span style="color: gray;">
            My one-year-old pet cat, "Sprigatito" (who can't do backflips).
        </span>
    </aside>
    <!-- Description -->
    <dl>
        <dt style="color: gray;">Name</dt>
        <dd>
            SpringRain
        <dd>
        <dt style="color: gray;">Field of Study</dt>
        <dd>
            Machine Learning (some), Information Security(not much)
        <dd>
        <dt style="color: gray;">Intro<dt>
        <dd>
            During the winter vacation of my sophomore year, I came into contact with information security by chance and became interested in it. I co-founded the NTU Information Security Society with hicat0x0. Enjoy writing documentation.
        </dd>
    </dl>
<br/><br/><br/><br/><br/><br/>
</div>



> ## **Public Relations Officer**
<div>
    <!-- Aside block -->
    <aside>
        <span style="font-size: larger;">山高</span>
        {{
            image(
                url      = "images/known.png",
                alt      = "Public Relations Officer",
                no_hover = true
            )           
        }}
        <span style="color: gray;">
            Ahhhhhhhhhhhhh!
        </span>
    </aside>
    <!-- Description -->
    <dl>
        <dt style="color: gray;">Name</dt>
        <dd>
            山高 (shan gao)
        <dd>
        <dt style="color: gray;">Field of Study</dt>
        <dd>
            Graphic design, digital signal processing
        <dd>
        <dt style="color: gray;">Intro<dt>
        <dd>
            A very, very high mountain
        </dd>
    </dl>
<br/><br/><br/><br/><br/><br/>
</div>



> ## **Treasurer**
<div>
    <!-- Aside block -->
    <aside>
        <span style="font-size: larger;">Kami</span>
        {{
            image(
                url      = "images/treasurer_en.png",
                alt      = "treasurer",
                no_hover = true
            )           
        }}
        <span style="color: gray;">
            Don't trust that cat have a golden cycle on it's ears.
        </span>
    </aside>
    <!-- Description -->
    <dl>
        <dt style="color: gray;">Name</dt>
        <dd>
            Kami
        <dd>
        <dt style="color: gray;">Field of Study</dt>
        <dd>
            Algorithms (just a little), paying OpenAI(a lot).
        <dd>
        <dt style="color: gray;">Intro<dt>
        <dd>
            Prefer LL over LeetCode, passively signed up to become a member.
        </dd>
    </dl>
<br/><br/><br/><br/><br/><br/>
</div>



> ## **Network Administrator**
<div>
    <!-- Aside block -->
    <aside>
        <span style="font-size: larger;">Sam Doner</span>
        {{
            image(
                url      = "images/doner.png",
                alt      = "Network Administrator",
                no_hover = true
            )           
        }}
        <span style="color: gray;">
            This is 
                <a href="https://zh.wikipedia.org/zh-tw/%E5%9C%9F%E8%80%B3%E5%85%B6%E7%83%A4%E8%82%89">
                Döner
                </a>
            , not a 
                <a href="https://zh.wikipedia.org/zh-tw/%E5%9C%9F%E8%80%B3%E5%85%B6%E7%83%A4%E8%82%89">
                Burrito
                </a>
        </span>
    </aside>
    <!-- Description -->
    <dl>
        <dt style="color: gray;">Name</dt>
        <dd>
            Sam Doner
        <dd>
        <dt style="color: gray;">Field of Study</dt>
        <dd>
            Play Minecraft
        <dd>
        <dt style="color: gray;">Intro<dt>
        <dd>
            This fool is just a piece of barbecue, leave him alone.
        </dd>
    </dl>
<br/><br/><br/><br/><br/><br/>
</div>




<!-- Buttons -->
<div class="dialog-buttons">
  <a class="inline-button" href="#top">Go to Top</a>
</div>